# Mass Flow Controller MKS GV50A

**DEPRECATED**
**Please use vac_ctrl_mks946_937b with the built-in vac_mfc_mks_gv50a snippet**

EPICS module to read/write data for MKS GV50A mass flow controller (MFC).

IOC does not directly communicate with the MFC, the gauge is actully connected to a vacuum controller. IOC communicated to the controller and controller provides MFC data. Controllers can have multiple gauges and MFCs in different channels.


The modules arrangement should be something like:
```
IOC-|
    |-vac-ctrl-mks946_937b
	|	   |-vac-gauge-mks-vgp (Pirani gauge)
	|	   |-vac-gauge-mks-vgp (Pirani gauge)
	|      |-vac-gauge-mks-vgc (Cold cathode gauge)
	|
	|-vac-ctrl-mks946_937b
	|	   |-vac-gauge-mks-vgp (Pirani gauge)
	|	   |-vac-mfc-mks-gv50a (Mass flow controller)
```

This allows EPICS modules for gauges to be re-used and any combination of controller and gauges to be built from existing modules.

**This version of the module requires at least v3.0.4 of vac_ctrl_mks946_937b**

## Startup Examples

`iocsh -r vac_ctrl_mks946_937b,catania -c 'requireSnippet(vac_ctrl_mks946_937b_ethernet.cmd,"DEVICENAME=LNS-LEBT-010:VAC-VEVMC-01100, IPADDR=10.4.0.213, PORT=4004")' -r vac_mfc_mks_gv50a,catania -c 'requireSnippet(vac_mfc_mks_gv50a.cmd, "DEVICENAME=LNS-LEBT-010:VAC-VVMC-01100, CONTROLLERNAME=LNS-LEBT-010:VAC-VEVMC-01100, CHANNEL=5")'`

If ran as proper ioc service:
```
epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VVMC-04100")
epicsEnvSet(CONTROLLERNAME, "LNS-LEBT-010:VAC-VEVMC-01100")
epicsEnvSet(CHANNEL, "2")
require vac_mfc_mks_gv50a, 2.0.4-catania
< ${REQUIRE_vac_mfc_mks_gv50a_PATH}/startup/vac_mfc_mks_gv50a.cmd
```
